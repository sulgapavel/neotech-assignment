package io.javastamp.JavaStamp;

import com.coreoz.wisp.Scheduler;
import com.coreoz.wisp.schedule.Schedules;
import io.javastamp.JavaStamp.db.Database;
import io.javastamp.JavaStamp.db.DatabaseConnectionTimer;

import java.sql.Statement;
import java.time.Duration;
import java.util.ArrayDeque;

public class Writer {

    private Database db;

    private final ArrayDeque<Long> buffer = new ArrayDeque<>();

    private void onDatabaseReconnect() {
        System.err.println("Database reconnected. Dumping instants from buffer to database");
        synchronized (buffer) {
            buffer.forEach(this::writeInstantToDb);
            buffer.clear();
        }
    }

    private void writeInstantToDb(Long instant) {
        System.err.println("Writing instant " + instant + " to database");

        try {
            Statement statement = db.getConnection().createStatement();

            statement.execute("INSERT INTO `instants` (`instant`) VALUES ("+ instant +")");
        } catch (Exception e) {
            System.err.println("Seems like database connection is lost.");
            db.closeConnection();
            writeInstantToBuffer(instant);
        }
    }

    private void writeInstantToBuffer(Long instant) {
        System.err.println("Writing instant "+instant+" to buffer...");
        synchronized (buffer) {
            buffer.add(instant);
        }
    }

    private void write() {

        Long instant = System.nanoTime();

        if (db.isConnected()) {
            writeInstantToDb(instant);
        } else {
            writeInstantToBuffer(instant);
        }
    }

    public void launch(Database db, DatabaseConnectionTimer timer) throws InterruptedException {
        this.db = db;
        timer.start(db, this::onDatabaseReconnect);

        new Scheduler().schedule(this::write, Schedules.fixedDelaySchedule(Duration.ofSeconds(1)));

        Thread.sleep(Long.MAX_VALUE);
    }
}