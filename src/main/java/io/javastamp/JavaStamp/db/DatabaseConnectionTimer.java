package io.javastamp.JavaStamp.db;

import com.coreoz.wisp.Scheduler;
import com.coreoz.wisp.schedule.Schedules;

import java.time.Duration;

public class DatabaseConnectionTimer {

    private void tick(Database database, Runnable onConnected) {
        if(!database.isConnected()) {
            System.err.println("Database is not connected. Trying to reconnect");
            boolean success = database.connect();

            if(success) {
                onConnected.run();
                System.err.println("Reconnected!");
            } else {
                System.err.println("Failed to reconnect. Next attempt in 5s...");
            }
        }
    }

    public void start(Database database, Runnable onConnected) {
        new Scheduler().schedule(() -> tick(database, onConnected), Schedules.fixedDelaySchedule(Duration.ofSeconds(5)));
    }
}
