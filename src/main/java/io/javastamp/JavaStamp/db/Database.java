package io.javastamp.JavaStamp.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

public class Database {

    private String host;
    private String port;
    private String user;
    private String pass;
    private String name;
    private String path;

    private String getString(Map map, String path) {
        Object host = map.get(path);

        if (host == null || host == "todo") {
            System.err.println("Invalid db configuration: (" + path + "): " + host);
            System.exit(1);
        }

        return host.toString();
    }

    public Database(Map properties) {
        this.host = getString(properties, "host");
        this.port = getString(properties, "port");
        this.user = getString(properties, "user");
        this.pass = getString(properties, "pass");
        this.name = getString(properties, "name");

        this.path = "jdbc:mysql://" + host + ":" + port + "/" + name;
    }

    private Connection createConnection() throws SQLException {
        return DriverManager.getConnection(path, user, pass);
    }

    private Connection connection;

    private boolean createTableIfNotExists(Connection connection) {
        try {
            connection.createStatement().execute(
                    "CREATE TABLE IF NOT EXISTS instants (\n" +
                            "`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,\n" +
                            "instant BIGINT NOT NULL\n" +
                            ")"
            );
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    public boolean connect() {
        System.err.println("Trying to connect: " + path + " with user " + user + " and pass " + pass);
        try {
            Connection connection = createConnection();
            if (connection == null) {
                System.err.println("Connection is null.");
                return false;
            }
            this.connection = connection;
            return createTableIfNotExists(connection);
        } catch (SQLException e) {
            System.err.println("Connection failed with exception: " + e);
            return false;
        }
    }

    public boolean isConnected() {
        return connection != null;
    }

    public Connection getConnection() {
        return connection;
    }

    public void closeConnection() {
        connection = null;
    }
}
