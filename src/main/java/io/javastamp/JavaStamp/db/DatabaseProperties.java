package io.javastamp.JavaStamp.db;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Properties;

public class DatabaseProperties {

    private static Properties defaultProperties;

    static {
        defaultProperties = new Properties();
        defaultProperties.put("host", "todo");
        defaultProperties.put("port", "todo");
        defaultProperties.put("user", "todo");
        defaultProperties.put("pass", "todo");
        defaultProperties.put("name", "todo");
    }

    public Map<Object, Object> load() throws IOException {

        Path path = Paths.get("./db.properties");
        if (Files.exists(path)) {
            Properties properties = new Properties(defaultProperties);

            properties.load(Files.newBufferedReader(path));

            return properties;
        } else {
            defaultProperties.store(Files.newBufferedWriter(path), "Please configure database connection");
            System.err.println("Please configure database connection file (" + path.toAbsolutePath().toString() + ")");
            System.exit(1);
            return null;
        }
    }
}
