package io.javastamp.JavaStamp;

import io.javastamp.JavaStamp.db.Database;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Duration;

public class Dumper {
    public void dump(Database database) throws SQLException {
        ResultSet resultSet = database
                .getConnection()
                .createStatement()
                .executeQuery("SELECT (`id`,`instant`) FROM `instants`");

        System.err.println("Database dump:");

        while (resultSet.next()) {
            Date date = new Date(Duration.ofNanos(resultSet.getLong("instant")).toMillis());
            System.err.println(date.toString());
        }
    }
}
