package io.javastamp.JavaStamp;

import io.javastamp.JavaStamp.db.Database;
import io.javastamp.JavaStamp.db.DatabaseConnectionTimer;
import io.javastamp.JavaStamp.db.DatabaseProperties;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;

public class Main {

    private static Database getDatabase() {
        try {
            return new Database(new DatabaseProperties().load());
        } catch (IOException e) {
            System.err.println("Failed to load DB properties");
            e.printStackTrace();
            System.exit(1);
            return null;
        }
    }

    public static void main(String[] args) throws InterruptedException {

        boolean print = Arrays.asList(args).contains("-p");
        Database database = getDatabase();
        DatabaseConnectionTimer timer = new DatabaseConnectionTimer();
        database.connect();

        if (print) {
            if(!database.isConnected()) {
                System.err.println("Database is not connected. Can't dump data.");
                System.exit(1);
            } else {
                try {
                    new Dumper().dump(database);
                } catch (SQLException e) {
                    System.err.println("DB error");
                    e.printStackTrace();
                }
            }
        } else {
            new Writer().launch(database, timer);
        }
    }
}
